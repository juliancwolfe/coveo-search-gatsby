Getting started:

1. `make vendor`
2. `cp .env.dist .env.development`
3. Add the `COVEO_API_TOKEN` value so that the API works in `.env.development`
4. `make develop` to run http://localhost:8000

Production build:

1. `make build`

To test serve: `make serve` -> http://localhost:9000

