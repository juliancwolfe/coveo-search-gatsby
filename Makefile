vendor:
	npm install

develop:
	./node_modules/.bin/gatsby develop

clean:
	rm -rf .cache public

env:
	set -a; source .env; set +a;

build: clean env
	./node_modules/.bin/gatsby build

serve: build
	./node_modules/.bin/gatsby serve