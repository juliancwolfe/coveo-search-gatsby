export default {
    "host": process.env.COVEO_API_HOST,
    "port": process.env.COVEO_API_PORT,
    "path": process.env.COVEO_API_PATH,
    "scheme": process.env.COVEO_API_SCHEME,
    "respondAsText": false,
    "headers": {
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json",
    },
    "token": process.env.COVEO_API_TOKEN,
};
