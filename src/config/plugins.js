const siteConfig = require('./site');
const styles = require('./styles')

module.exports = [
  {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: siteConfig.title,
        short_name: siteConfig.title,
        start_url: '/',
        background_color: '#FFF',
        theme_color: '#F7A046',
        display: 'standalone',
      //   icon: 'static/photo.jpg'
        },
    },
    {
      resolve: `gatsby-plugin-env-variables`,
      options: {
        whitelist: ["COVEO_API_SCHEME", "COVEO_API_HOST", "COVEO_API_PORT", "COVEO_API_PATH", "COVEO_API_TOKEN"]
      },
    },
    styles,
];