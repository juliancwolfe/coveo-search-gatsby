import { Result } from "../entities";

export default class ResultFactory {
    create(data) {
        const {
            uniqueId,
            title,
            uri,
            rating,
            raw,
        } = data;

        const {
            tpthumbnailuri,
            tpprixnum,
            tpproducteur,
            tpformat,
            tppays,
            tpcategorie,
            tpsousregion,
            tppastilledegoutsplitgroup,
            tpobservationsgustavivesfamillesaromessplitgroup,
        } = raw;

        return new Result()
            .setId(uniqueId)
            .setTitle(title)
            .setUri(uri)
            .setRating(rating)
            .setThumbnailUri(tpthumbnailuri)
            .setPrice(tpprixnum)
            .setProducer(tpproducteur)
            .setFormat(tpformat)
            .setCountry(tppays)
            .setRegion(tpsousregion)
            .setCategory(tpcategorie)
            .setTasteLabels(this.extractTerms(tppastilledegoutsplitgroup))
            .setAromaFamilies(this.extractTerms(tpobservationsgustavivesfamillesaromessplitgroup))
        ;
    }

    extractTerms(itemString = '', delimiter = ';') {
        return itemString.split(delimiter).filter(i => i !== '').map(item => this.removePunctuation(item));
    }

    removePunctuation(str) {
        return str.replace(/\.*,*/g, "");
    }
}