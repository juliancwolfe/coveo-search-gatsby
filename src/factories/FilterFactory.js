import { Filter } from "../entities";

export default class FilterFactory {
    create(data) {
        const {
            field,
            values,
        } = data;

        return new Filter()
            .setField(field)
            .setValues(values.map(({value}) => { return {value}}))
        ;
    }
}