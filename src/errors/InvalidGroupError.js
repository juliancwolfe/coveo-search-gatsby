export default class InvalidGroupError extends Error {
    constructor() {
        super('The group passed must be an instance of Group');
    }
}