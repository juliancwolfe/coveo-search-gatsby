export default class AbstractMethodError extends Error {
    constructor(method) {
        super(`The "${method}" method must be implemented`);
    }
}