export default class InvalidCriteriaError extends Error {
    constructor() {
        super('The Criteria passed must be an instance of Criteria');
    }
}