export default class InvalidQueryBuilderError extends Error {
    constructor() {
        super('A valid QueryBuilder must be passed.');
    }
}