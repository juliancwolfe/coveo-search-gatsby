export default class InvalidSortError extends Error {
    constructor() {
        super('The Sort passed must be an instance of Sort');
    }
}