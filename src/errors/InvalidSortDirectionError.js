export default class InvalidSortDirectionError extends Error {
    constructor(direction) {
        super(`The direction ${direction} is not a valid value`);
    }
}