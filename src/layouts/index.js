import IndexLayout from './IndexLayout';
import NotFoundLayout from './NotFoundLayout';

export {
    IndexLayout,
    NotFoundLayout,
}