import React, {PureComponent} from "react"
import { Page } from "../../components/common";

export default class NotFoundLayout extends PureComponent {
    static defaultProps = {
        styles: {},
    };

    render() {
        const {
            title,
            description,
            styles,
        } = this.props;

        return (
            <Page title={title} description={description} styles={styles['page']}>
                <h1 className={styles['header']}>NOT FOUND</h1>
                <p className={styles['message']}>You just hit a route that doesn&#39;t exist... the sadness.</p>
            </Page>
        );
    }
}
