import React, { PureComponent } from 'react';
import { NavBar, Showcase } from '../../components/sections';
import { MenuAwareLayout } from '../../components/layouts';
import { Search, QueryBuilder as AdvancedSearch } from '../../components/input';
import { Button } from 'semantic-ui-react';
import { Pagination, Page } from '../../components/common';

export default class IndexLayout extends PureComponent {
    state = {
        showAdvancedSearch: true,
    }

    render() {
        const {
            items, 
            qb, 
            limitValue,
            categories,
            countries,
            stores,
            limitValues,
            handleSearch,
            refreshResults,
        } = this.props;

        const {
            showAdvancedSearch,
        } = this.state;

        return (
            <Page>
                <MenuAwareLayout>
                    <NavBar>
                        <Search onSubmit={query => handleSearch(query)}/>
                        <Button icon='filter' toggle onClick={() => this.toggleAdvancedSearch()}/>
                        {items && (
                            <Pagination 
                                page={qb && qb.getPage()} 
                                totalPages={Math.ceil((items && items.getTotal())/(qb && qb.getLimit()))} 
                                onPageChange={page => refreshResults(qb.setPage(page))}
                            />
                        )}
                    </NavBar>
                </MenuAwareLayout>
                <AdvancedSearch 
                    qb={qb} 
                    onClick={() => refreshResults(qb)} 
                    hidden={!showAdvancedSearch} 
                    limitValues={limitValues} 
                    limitValue={limitValue} 
                    totalResults={items && items.getTotal()}
                    categories={categories}
                    countries={countries}
                    stores={stores}
                />
                <Showcase items={items}></Showcase>
            </Page>
        );
    }

    toggleAdvancedSearch() {
        const {showAdvancedSearch} = this.state;
        this.setState({showAdvancedSearch: !showAdvancedSearch});
    }
}