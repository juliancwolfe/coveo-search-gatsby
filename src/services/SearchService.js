import { QueryBuilder } from './search';
import { InvalidQueryBuilderError } from '../errors';
import { Group } from './search/group';

export default class SearchService {
    constructor(repository, filterRepository, config = {}) {
        this.repository = repository;
        this.filterRepository = filterRepository;
        this.config = config;
    }

    async search(queryBuilder) {
        if (!(queryBuilder instanceof QueryBuilder)) {
            throw new InvalidQueryBuilderError();
        }

        return await this.repository.get(queryBuilder.build());
    }

    createQueryBuilder() {
        const {fieldsToInclude} = this.config;

        return new QueryBuilder()
            .setFieldsToInclude(fieldsToInclude);
    }

    async getFilters(field, limit) {
        let queryBuilder = new QueryBuilder()
            .setLimit(0)
            .setQuery('@sysuri')
            .upsertGroupBy(new Group(field).setLimit(limit).setSortCriteria('occurences'))
        ;

        return await this.filterRepository.get(queryBuilder.build());
    }
}