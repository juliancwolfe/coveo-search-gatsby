import Criteria from './Criteria';

export default class RangeCriteria extends Criteria {
    minValue;
    maxValue;

    /**
     * 
     * @param {string} field 
     */
    constructor(field) {
        super(field);
    }

    buildValue() {
        return `${this.minValue}..${this.maxValue}`;
    }

    getMinValue() {
        return this.minValue;
    }

    /**
     * 
     * @param {number} minValue 
     */
    setMinValue(minValue) {
        this.minValue = minValue;
        return this;
    }

    getMaxValue() {
        return this.maxValue;
    }

    /**
     * 
     * @param {number} maxValue 
     */
    setMaxValue(maxValue) {
        this.maxValue = maxValue;
        return this;
    }
}