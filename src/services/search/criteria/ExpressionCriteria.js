import Criteria from './Criteria';

export default class ExpressionCriteria extends Criteria {
    value;

    /**
     * 
     * @param {string} field 
     */
    constructor(field) {
        super(field);
    }

    buildValue() {
        return this.value;
    }

    getOperator() {
        return this.operator;
    }

    setOperator(operator) {
        this.operator = operator;
        return this;
    }

    getValue() {
        return this.value();
    }

    setValue(value) {
        this.value = value;
        return this;
    }
}