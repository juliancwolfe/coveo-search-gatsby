import Criteria from './Criteria';

export default class SetCriteria extends Criteria {
    criteria = new Map();

    /**
     * 
     * @param {string} field 
     */
    constructor(field) {
        super(field);
    }

    buildValue() {
        const list = Array.from(this.criteria.values()).map(item => `"${item}"`).join(',');
        return `(${list})`;
    }

    /**
     * Adding a criteria is O(1)
     * @param {string} item 
     */
    addCriterion(item) {
        this.criteria.set(item, item);
        return this;
    }

    /**
     * Ensures that removal is O(1) by using a Map
     * @param {string} item 
     */
    removeCriterion(item) {
        this.criteria.delete(item);
        return this;
    }

    resolveCriteria(items = []) {
        const criteria = Array.from(this.criteria.values());

        const removedItems = criteria.filter(criterion => !items.includes(criterion));
        const addedItems = items.filter(criterion => !criteria.includes(criterion));

        for (let item of Object.values(removedItems)) {
            this.removeCriterion(item);
        }

        for (let item of Object.values(addedItems)) {
            this.addCriterion(item);
        }

        return this;
    }

    /**
     * 
     * @param {[]} items1 
     * @param {[]} items2 
     */
    diff(items1, items2) {
        return items1.filter(item => items2.indexOf(item) < 0);
    }
}