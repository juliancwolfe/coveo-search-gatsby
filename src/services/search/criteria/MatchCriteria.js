import Criteria from './Criteria';

export default class MatchCriteria extends Criteria {
    value;

    /**
     * 
     * @param {string} field 
     */
    constructor(field) {
        super(field);
    }

    buildValue() {
        return `"${this.value}"`;
    }

    getValue() {
        return this.value();
    }

    setValue(value) {
        this.value = value;
        return this;
    }
}