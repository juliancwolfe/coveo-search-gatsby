import Criteria from './Criteria';

export default class BooleanCriteria extends Criteria {
    value;

    /**
     * 
     * @param {string} field 
     */
    constructor(field) {
        super(field);
    }

    buildValue() {
        return JSON.stringify(this.value);
    }

    getValue() {
        return this.value;
    }

    /**
     * 
     * @param {boolean} value 
     */
    setValue(value) {
        this.value = value;
        return this;
    }
}