import { AbstractMethodError } from "../../../errors";

export default class Criteria {
    /**
     * @property string field
     */
    field;
    operator = '==';

    /**
     * 
     * @param {string} field 
     */
    constructor(field) {
        this.field = field;
    }

    /**
     * @returns string
     */
    build() {
        return `(@${this.field}${this.operator}${this.buildValue()})`;
    }

    /**
     * @abstract
     */
    buildValue() {
        throw new AbstractMethodError("buildValue");
    }

    /**
     * @returns string
     */
    getField() {
        return this.field;
    }
}