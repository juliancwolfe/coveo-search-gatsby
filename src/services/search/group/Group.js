export default class Group {
    field;
    limit;
    sortCriteria;
    injectionDepth;
    allowedValues = new Map();
    
    constructor(field) {
        this.field = field;
    }

    getField() {
        return this.field;
    }

    getLimit() {
        return this.limit;
    }

    setLimit(limit) {
        this.limit = limit;
        return this;
    }

    addAllowedValue(value) {
        this.allowedValues.set(value, value);
        return this;
    }

    removeAllowedValue(value) {
        this.allowedValues.delete(value);
        return this;
    }

    getAllowedValues() {
        return Array.from(this.allowedValues.values());
    }

    getSortCriteria() {
        return this.sortCriteria;
    }

    setSortCriteria(sortCriteria) {
        this.sortCriteria = sortCriteria;
        return this;
    }

    build() {
        return {
            field: `@${this.field}`,
            maximumNumberOfValues: this.limit,
            sortCriteria: this.sortCriteria,
            allowedValues: this.getAllowedValues(),
        }
    }
}