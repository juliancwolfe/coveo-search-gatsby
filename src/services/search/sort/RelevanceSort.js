import Sort from "./Sort";

export default class RelevanceSort extends Sort {
    constructor(field, type) {
        super(field, type);
    }

    getValue() {
        return 'relevancy';
    }

    build() {
        return this.getValue();
    }
}