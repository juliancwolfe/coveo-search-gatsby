import Sort from "./Sort";

export default class DateSort extends Sort {
    constructor(field, type) {
        super(field, type);
    }

    getValue() {
        return `date${this.direction}`;
    }

    build() {
        return `date ${this.direction}`;
    }
}