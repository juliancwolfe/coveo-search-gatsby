import { InvalidSortDirectionError } from "../../../errors";

export default class Sort {
    static DIRECTION_ASCENDING = 'ascending';
    static DIRECTION_DESCENDING = 'descending';

    field;
    direction;

    constructor(field) {
        this.field = field;
    }

    build() {
        return `@${this.field} ${this.direction}`;
    }

    getValue() {
        return `field${this.direction}`;
    }

    getField() {
        return this.field;
    }

    getDirection() {
        return this.direction;
    }

    setDirection(direction) {
        if (![Sort.DIRECTION_ASCENDING, Sort.DIRECTION_DESCENDING].includes(direction)) {
            throw new InvalidSortDirectionError(direction);
        }

        this.direction = direction;
        return this;
    }
}