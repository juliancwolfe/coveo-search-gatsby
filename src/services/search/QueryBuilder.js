import { Criteria } from "./criteria";
import { InvalidCriteriaError, InvalidSortError, InvalidGroupError } from "../../errors";
import { Sort } from "./sort";
import { Group } from "./group";

export default class QueryBuilder {
    criteria = new Map();
    limit;
    page;
    sort = new Map();
    fieldsToInclude;
    fieldsToExclude;
    groupBy = new Map();

    upsertCriteria(criteria) {
        if (!(criteria instanceof Criteria)) {
            throw new InvalidCriteriaError();
        }

        this.criteria.set(criteria.getField(), criteria);
        return this;
    }

    removeCriteria(criteria) {
        if (!(criteria instanceof Criteria)) {
            throw new InvalidCriteriaError();
        }

        this.criteria.delete(criteria.getField());
        return this;
    }

    getLimit() {
        return this.limit;
    }

    setLimit(limit) {
        this.limit = limit;
        return this;
    }

    getPage() {
        return this.page;
    }

    setPage(page) {
        this.page = page;
        return this;
    }

    setQuery(query) {
        this.query = query;
        return this;
    }

    setRetrieveFirstSentances(retrieveFirstSentances = true) {
        this.retrieveFirstSentances = retrieveFirstSentances;
        return this;
    }
    
    setEnableDuplicateFiltering(enableDuplicateFiltering = true) {
        this.enableDuplicateFiltering = enableDuplicateFiltering;
        return this;
    }

    setEnableDidYouMean(enableDidYouMean = true) {
        this.enableDidYouMean = enableDidYouMean;
        return this;
    }

    setFieldsToInclude(fieldsToInclude = []) {
        this.fieldsToInclude = fieldsToInclude;
        return this;
    }

    setFieldsToExclude(fieldsToExclude = []) {
        this.fieldsToExclude = fieldsToExclude;
        return this;
    }

    upsertSort(sort) {
        if (!(sort instanceof Sort)) {
            throw new InvalidSortError();
        }

        this.sort.set(sort.getField(), sort);
        return this;
    }

    removeSort(sort) {
        if (!(sort instanceof Sort)) {
            throw new InvalidSortError();
        }

        this.sort.delete(sort.getField());
        return this;
    }

    upsertGroupBy(group) {
        if (!(group instanceof Group)) {
            throw new InvalidGroupError();
        }

        this.groupBy.set(group.getField(), group);
        return this;
    }

    removeGroupBy(group) {
        if (!(group instanceof Group)) {
            throw new InvalidGroupError();
        }

        this.groupBy.delete(group.getField());
        return this;
    }

    build() {
        return {
            sortCriteria: this.buildSortCriteria(),
            numberOfResults: this.limit,
            firstResult: this.calculateFirstResult(this.limit, this.page),
            aq: this.buildAdvancedQuery(),
            q: this.query,
            retrieveFirstSentances: this.retrieveFirstSentances,
            enableDuplicateFiltering: this.enableDuplicateFiltering,
            enableDidYouMean: this.enableDidYouMean,
            fieldsToInclude: this.fieldsToInclude,
            fieldsToExclude: this.fieldsToExclude,
            groupBy: this.buildGroupBy(),
        }
    }

    calculateFirstResult(limit = 1, page = 1) {
        return limit * (page - 1);
    }

    buildAdvancedQuery() {
        return Array.from(this.criteria.values()).map(criteria => criteria.build()).join(' ');
    }

    buildSortCriteria() {
        return Array.from(this.sort.values()).map(sort => sort.build()).join(', ');
    }

    buildGroupBy() {
        return Array.from(this.groupBy.values()).map(group => group.build());
    }
}