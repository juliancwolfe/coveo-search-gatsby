import axios from 'axios';

export default class ApiProvider {
    constructor(config = {}) {
        this.config = config;

        const {host, path, port, token, scheme} = this.config;

        this.client = axios.create({
            baseURL: this.buildUrl(host, path, port, scheme),
            headers: {
                Authorization: `Bearer ${token}`
            },
        })
    }

    async get(path = '', params = {}) {
        return await this.client.get(path, {params});
    }

    async post(path = '', body = {}) {
        return await this.client.post(path, body);
    }

    buildUrl(host, path, port, scheme = 'https') {
        return `${scheme}://${host}${port ? `:${port}` : ''}/${path}`;
    }
}