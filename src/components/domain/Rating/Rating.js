import React, { PureComponent } from 'react';
import { Rating as SemanticRating } from 'semantic-ui-react';

export default class Rating extends PureComponent {
    static defaultProps = {
        styles: {},
    };

    render() {
        const {styles, rating} = this.props;

        return (
            <div className={styles['rating']}>
                <SemanticRating defaultRating={rating} maxRating={5} disabled/>
            </div>
        );
    }
}