import React, { PureComponent } from 'react';
import { Card } from 'semantic-ui-react';
import Result from '../Result';

export default class ResultCollection extends PureComponent {
    static defaultProps = {
        styles: {},
    };

    render() {
        const {
            styles, 
            items, 
            children,
        } = this.props;

        return (
            <div className={styles['resultcollection']}>
                <Card.Group centered>
                    {this.prepareItems(items) || children}
                </Card.Group>
            </div>
        );
    }

    prepareItems(items) {
        if (!items) {
            return;
        }

        let components = [];

        for (let item of items.getValues()) {
            components.push(<Result {...item}/>);
        }

        return components;
    }
}