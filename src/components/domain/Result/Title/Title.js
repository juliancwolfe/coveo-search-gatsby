import React, { PureComponent } from 'react';
import { Card } from 'semantic-ui-react';

import './Title.scss';

export default class Title extends PureComponent {
    render() {
        const {
            title, 
            uri,
            producer,
        } = this.props;

        return (
            <Card.Content className="title">
                <Card.Header><a href={uri} hrefLang={'fr'} target="_blank" rel="noopener noreferrer">{title}</a></Card.Header>
                <Card.Meta>{producer}</Card.Meta>
            </Card.Content>
        );
    }
}