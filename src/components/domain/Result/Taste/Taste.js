import React, { PureComponent } from 'react';
import { Card, Label } from 'semantic-ui-react';
import './Taste.scss';

export default class Taste extends PureComponent {
    prepareLabelContent(tasteLabels = [], aromaFamilies = []) {
        const items = [...tasteLabels, ...aromaFamilies];
        return items.filter((item, position) => items.indexOf(item === position)).filter(i => i);
    }

    normalizeString(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    }

    render() {
        const {tasteLabels, aromaFamilies} = this.props;
        const labels = this.prepareLabelContent(tasteLabels, aromaFamilies).map(item => <Label className={this.normalizeString(item.toLowerCase())}>{item}</Label>);

        if (!labels.length) {
            return '';
        }

        return (
            <Card.Content extra textAlign="center" className="taste">
                <Label.Group size='mini' circular>
                    {labels}
                </Label.Group>
            </Card.Content>
        );
    }
}