import React, { PureComponent } from 'react';
import { Card, Label } from 'semantic-ui-react';

export default class Meta extends PureComponent {
    render() {
        const {format, country, category} = this.props;

        if (!(format || country || category)) {
            return '';
        }

        return (
            <Card.Content extra textAlign="center">
                <Label.Group color='black' size='mini'>
                    {format && <Label>{format}</Label>}
                    {country && <Label>{country}</Label>}
                    {category && <Label>{category}</Label>}  
                </Label.Group>
            </Card.Content>
        );
    }
}