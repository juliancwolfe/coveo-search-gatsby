import React, { PureComponent } from 'react';
import { Image } from 'semantic-ui-react';
import './Thumbnail.scss';

export default class Thumbnail extends PureComponent {
    render() {
        const {uri, alt} = this.props;

        return (
            <div className={'thumbnail'}>
                <Image 
                    src={uri} 
                    wrapped 
                    ui={false} 
                    alt={alt} 
                    verticalAlign={'middle'}
                />
            </div>
        );
    }
}