import React, { PureComponent } from 'react';
import { Card } from 'semantic-ui-react';
import Thumbnail from './Thumbnail';
import Title from './Title';
import Purchase from './Purchase';
import Meta from './Meta';
import Taste from './Taste';

import './Result.scss';

export default class Result extends PureComponent {
    render() {
        const {
            title, 
            uri, 
            rating,
            thumbnailUri,
            price,
            producer,
            format,
            country,
            region,
            category,
            tasteLabels,
            aromaFamilies,
        } = this.props;

        return (
            <div className="result">
                <Card>
                    <Thumbnail uri={thumbnailUri} alt={title} />
                    <Title title={title} uri={uri} producer={producer}/>
                    <Taste tasteLabels={tasteLabels} aromaFamilies={aromaFamilies}/>
                    <Meta format={format} country={country} region={region} category={category}/>
                    <Purchase price={price} rating={rating}/>
                </Card>
            </div>
        );
    }
}