import React, { PureComponent } from "react";
import { Card, Grid } from 'semantic-ui-react';
import { Price } from '../../../common';
import Rating from '../../Rating';

export default class Purchase extends PureComponent {
    render() {
        const {
            rating,
            price,
        } = this.props;

        return (
            <Card.Content extra>
                <Grid columns={2} divided>
                    <Grid.Row>
                        <Grid.Column><Rating rating={rating}/></Grid.Column>
                        <Grid.Column textAlign='right'><Price price={price} additional="77ml"/></Grid.Column>
                    </Grid.Row>
                </Grid>
            </Card.Content>
        );
    }
}