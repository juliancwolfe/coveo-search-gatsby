import React, { PureComponent } from "react";
import { Button } from "semantic-ui-react";
import { RelevanceSort } from "../../../../services/search/sort";

export default class SortRelevance extends PureComponent {
    static defaultProps = {
        onClick: () => {},
        field: 'RELEVANCE',
    };

    state = {
        active: true,
    }

    render() {
        const {label} = this.props;
        const {active} = this.state;

        return (
            <Button onClick={() => this.handleClick()} toggle={active}>{label}</Button>
        )
    }

    handleClick() {
        const {qb, field, onClick} = this.props;
        const {active} = this.state;

        const newActiveState = !active;

        let sort = new RelevanceSort(field);

        if (newActiveState) {
            qb.upsertSort(sort);
        } else {
            qb.removeSort(sort);
        }

        this.setState({active: newActiveState}, () => onClick());
    }
}