import React, { PureComponent } from "react";
import { Icon, Button } from "semantic-ui-react";
import { Sort } from "../../../../services/search/sort";

export default class SortItem extends PureComponent {
    static defaultProps = {
        onClick: () => {},
    };

    state = {};

    render() {
        const {label} = this.props;
        const {direction} = this.state;

        return (
            <Button onClick={() => this.handleClick()}>{label}{direction && ' '}{this.getIcon(direction)}</Button>
        )
    }

    handleClick() {
        const {qb, field, onClick} = this.props;
        const {direction} = this.state;

        const newDirection = this.getNextState(direction);
        let sort = new Sort(field);

        if (newDirection) {
            qb.upsertSort(sort.setDirection(newDirection));
        } else {
            qb.removeSort(sort);
        }

        this.setState({direction: newDirection}, () => onClick());
    }

    getNextState(direction) {
        switch(direction) {
            case undefined: return Sort.DIRECTION_ASCENDING;
            case Sort.DIRECTION_ASCENDING: return Sort.DIRECTION_DESCENDING;
            case Sort.DIRECTION_DESCENDING: return undefined;
        }
    }

    getIcon(direction) {
        switch(direction) {
            case Sort.DIRECTION_ASCENDING: return <Icon name="caret up"/>;
            case Sort.DIRECTION_DESCENDING: return <Icon name="caret down"/>;
        }
    }
}