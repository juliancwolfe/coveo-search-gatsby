import React, { PureComponent } from 'react';
import { Button } from 'semantic-ui-react';
import SortItem from '../SortItem';
import SortRelevance from '../SortRelevance';

export default class Sort extends PureComponent {
    render() {
        const {qb, onClick} = this.props;

        return (
            <div className={'sort'}>
                <Button.Group>
                    <SortItem qb={qb} label="Prix" field="tpprixnum" onClick={onClick}/>
                    <SortItem qb={qb} label="Millesime" field="tpmillesime" onClick={onClick}/>
                    <SortRelevance qb={qb} onClick={onClick} label="Relevance"/>
                </Button.Group>
            </div>
        );
    }
}