import React, { PureComponent } from 'react';
import PriceCriterion from '../PriceCriterion';
import { Grid, GridColumn } from 'semantic-ui-react';
import SetCriterion from '../SetCriterion';
import { FilterCollection } from '../../../../collections';

export default class Criteria extends PureComponent {
    render() {
        const {qb, onChange, categories, countries, stores} = this.props;
        
        return (
            <div className={'criteria'}>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={4} verticalAlign="middle">
                            <PriceCriterion qb={qb} onChange={onChange}/>
                        </Grid.Column>
                        <Grid.Column width={4} verticalAlign="middle">
                            <SetCriterion 
                                qb={qb} 
                                options={this.prepareValues(categories)} 
                                placeholder="Choisir des catégories" 
                                label="Catégories" 
                                field="tpcategorie"
                                onChange={onChange}
                            />
                        </Grid.Column>
                        <Grid.Column width={4} verticalAlign="middle">
                            <SetCriterion 
                                qb={qb} 
                                options={this.prepareValues(countries)} 
                                placeholder="Choisir des pays" 
                                label="Pays" 
                                field="tppays"
                                onChange={onChange}
                            />
                        </Grid.Column>
                        <Grid.Column width={4} verticalAlign="middle">
                            <SetCriterion 
                                qb={qb} 
                                options={this.prepareValues(stores)} 
                                placeholder="Succursale" 
                                label="Succursale" 
                                field="tpinventairenomsuccursalesplitgroup"
                                onChange={onChange}
                            />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }

    /**
     * 
     * @param {FilterCollection} items 
     */
    prepareValues(items) {
        if (!(items instanceof FilterCollection)) {
            return [];
        }

        for (let item of items.getValues()) {
            return item.getValues().map(({value}) => {
                return {key: value, text: value, value: value};
            });
        }
    }
}