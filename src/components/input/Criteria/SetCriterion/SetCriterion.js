import React, { PureComponent } from 'react';
import { Dropdown } from 'semantic-ui-react';
import { SetCriteria } from '../../../../services/search/criteria';

export default class SetCriterion extends PureComponent {
    static defaultProps = {
        options: [],
        onChange: () => {}
    }

    onChange(values = []) {
        const {qb, field, onChange} = this.props;

        qb.upsertCriteria(new SetCriteria(field).resolveCriteria(values));
        onChange();
    }

    render() {
        const {placeholder, options, label} = this.props;

        return (
            <div className={'setcriterion'}>
                {label && <label basic>{label}</label>}
                <Dropdown
                    placeholder={placeholder}
                    fluid
                    multiple
                    search
                    selection
                    options={options}
                    clearable
                    onChange={(e, {value}) => this.onChange(value)}
                />
            </div>
        );
    }
}