import React, { PureComponent } from 'react';
import { Slider } from "react-semantic-ui-range";
import { RangeCriteria } from '../../../../services/search/criteria';
import "./PriceCriterion.scss";

export default class PriceCriterion extends PureComponent {
    static defaultProps = {
        max: 300,
        min: 0,
        step: 10,
        trackFillBackgroundColor: '#1A5E63',
        thumbBackgroundColor: '#F2F4F3',
        onChange: () => {},
    }

    state = {};

    render() {
        const {max, min, step, trackFillBackgroundColor, thumbBackgroundColor} = this.props;

        const {currentMin = min, currentMax = max} = this.state;

        const settings = {
            start: [min, max],
            min,
            max,
            step,
            onChange: value => this.onChange(value),
        };

        return (
            <div className={'pricecriterion'}>
                <label basic>Prix</label>
                <Slider 
                    multiple 
                    settings={settings} 
                    style={{ trackFill: { backgroundColor: trackFillBackgroundColor}, thumb: {backgroundColor: thumbBackgroundColor}}}
                />
                <span className="min">{min}$ {currentMin}$</span>
                <span className="max">{currentMax}$ {max}$</span>
            </div>
        );
    }

    onChange([min, max]) {
        const {qb, onChange} = this.props;

        qb.upsertCriteria(new RangeCriteria('tpprixnum').setMinValue(min).setMaxValue(max));

        this.setState({currentMin: min, currentMax: max})
        onChange();
    }
}