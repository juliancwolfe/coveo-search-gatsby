import React, { PureComponent } from 'react';
import { Input, Icon } from 'semantic-ui-react';
import './Search.scss';

export default class Search extends PureComponent {
    static defaultProps = {
        onSubmit: query => {},
    }
    state = {};

    render() {
        return (
            <div className={'search'}>
                <Input 
                    action={{ icon: 'search', onClick: () => this.submit(this.state.query)}}
                    placeholder='Search...' 
                    onKeyDown={e => this.handleKeyDown(e)} 
                    onChange={(e, {value}) => this.handleChange(value)}
                />
            </div>
        );
    }

    handleChange(query) {
        this.setState({query})
    }

    handleKeyDown(e) {
        if ('Enter' !== e.key) {
            return;
        }

        return this.submit(this.state.query);
    }

    submit(query) {
        return this.props.onSubmit(query);
    }
}