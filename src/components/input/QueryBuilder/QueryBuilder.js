import React, { PureComponent } from 'react';
import { Container, Segment, Grid, GridColumn, Statistic } from 'semantic-ui-react';
import { Criteria } from '../criteria';
import Sort from '../sort/Sort/Sort';
import './QueryBuilder.scss';
import { LimitSelector } from '../../common';

export default class QueryBuilder extends PureComponent {
    render() {
        const {
            qb, 
            onClick, 
            hidden, 
            limitValues, 
            limitValue, 
            totalResults = 0,
            categories,
            countries,
            stores,
        } = this.props;
        
        return (
            <div className={'querybuilder'} hidden={hidden}>
                <Container fluid>
                    <Segment padded>
                        <Segment vertical>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column width={9}>
                                        <Criteria 
                                            qb={qb} 
                                            onChange={onClick} 
                                            categories={categories}
                                            countries={countries}
                                            stores={stores}
                                        />
                                    </Grid.Column>
                                    <GridColumn width={2} id="sort">
                                        <Sort qb={qb} onClick={onClick}/>
                                    </GridColumn>
                                    <GridColumn width={3} floated='right' textAlign='right'>
                                        <Grid floated="right">
                                            <Grid.Column width={9} id="stats" textAlign="right" floated="right">
                                                <Statistic horizontal size='mini'>
                                                    <Statistic.Value>{totalResults}</Statistic.Value>
                                                    <Statistic.Label>résultats</Statistic.Label>
                                                </Statistic>
                                            </Grid.Column>
                                            <Grid.Column id="limit-selector" width={7}>
                                                <LimitSelector value={(qb && qb.getLimit()) || limitValue} values={limitValues} qb={qb} onChange={onClick}/>
                                            </Grid.Column>
                                        </Grid>
                                    </GridColumn>
                                </Grid.Row>
                            </Grid>
                        </Segment>
                    </Segment>
                </Container>
            </div>
        );
    }
}