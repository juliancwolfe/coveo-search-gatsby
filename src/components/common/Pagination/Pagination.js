import React, { PureComponent } from 'react';
import { Pagination as SemanticPagination, Icon } from 'semantic-ui-react';
import './Pagination.scss';

export default class Pagination extends PureComponent {
    static defaultProps = {
        onPageChange: value => value,
    };

    render() {
        const {page, totalPages, onPageChange} = this.props;

        return (
            <div className={'pagination'}>
                <SemanticPagination
                    defaultActivePage={page}
                    activePage={page}
                    ellipsisItem={null}
                    firstItem={{ content: <Icon name='angle double left' />, icon: true }}
                    lastItem={{ content: <Icon name='angle double right' />, icon: true }}
                    prevItem={{ content: <Icon name='angle left' />, icon: true }}
                    nextItem={{ content: <Icon name='angle right' />, icon: true }}
                    totalPages={totalPages}
                    onPageChange={(e, {activePage}) => onPageChange(activePage)}
                />
            </div>
        );
    }
}