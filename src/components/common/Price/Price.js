import React, { PureComponent } from 'react';
import { Statistic } from 'semantic-ui-react';

export default class Price extends PureComponent {
    static defaultProps = {
        styles: {},
    };

    render() {
        const {styles, price} = this.props;

        return (
            <div className={styles['price']}>
                <Statistic size='mini' horizontal>
                    <Statistic.Value>{price.toFixed(2)}$</Statistic.Value>
                </Statistic>
            </div>
        );
    }
}