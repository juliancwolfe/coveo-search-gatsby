import React, { PureComponent } from 'react';
import { Dropdown } from 'semantic-ui-react';

export default class LimitSelector extends PureComponent {
    static defaultProps = {
        onChange: value => value,
    };

    render() {
        const {label, value, values} = this.props;

        const options = values.map(value => {
            return {value, text: value, key: value};
        });

        return (
            <div className={'limitselector'}>
                {label && <label>{label}</label>}
                <Dropdown
                    button 
                    value={value} 
                    options={options}
                    onChange={(e, {value}) => this.onChange(value)}
                />            
            </div>
        );
    }

    onChange(value) {
        const {qb, onChange} = this.props;

        qb.setLimit(value);
        onChange(value);
    }
    
}