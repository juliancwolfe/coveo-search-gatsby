import React, { PureComponent } from 'react';
import { Visibility, Responsive } from 'semantic-ui-react';

export default class MenuAwareLayout extends PureComponent {
    static defaultProps = {
        styles: {},
        onBottomPassed: () => {},
        onBottomPassedReverse: () => {},
    };

    state = {
        menuIsFixed: false,
    }

    showFixedMenu() {
        return this.setState({ menuIsFixed: true }, () => this.props.onBottomPassed());
    }

    hideFixedMenu() {
        return this.setState({ menuIsFixed: false }, () => this.props.onBottomPassedReverse());
    }

    render() {
        const {styles, children} = this.props;
        const {menuIsFixed} = this.state;

        return (
            <div className={styles['menuawarelayout']}>
                <Responsive minWidth={Responsive.onlyTablet.minWidth}>
                    <Visibility
                        once={false}
                        onBottomPassed={() => this.showFixedMenu()}
                        onBottomPassedReverse={() => this.hideFixedMenu()}
                    >
                        {React.Children.map(children, child => React.cloneElement(child, {menuIsFixed}))}
                    </Visibility>
                </Responsive>
                <Responsive maxWidth={Responsive.onlyTablet.minWidth}>
                    {children}
                </Responsive>
            </div>
        );
    }
}