import React, { PureComponent } from 'react';
import { Menu } from 'semantic-ui-react';
import './Navbar.scss';

export default class NavBar extends PureComponent {
    static defaultProps = {
        styles: {},
    };

    prepareItems(children) {
        return React.Children.map(children, child => (
            <Menu.Item>
                {child}
            </Menu.Item>
        ));
    }

    render() {
        const {children, menuIsFixed} = this.props;

        return (
            <div className={'navbar'}>
                <Menu
                    fixed={menuIsFixed ? 'top' : null}
                    inverted={!menuIsFixed}
                    pointing={!menuIsFixed}
                    borderless
                >
                    <Menu.Item icon='cocktail'/>
                    {this.prepareItems(children)}
                </Menu>
            </div>
        );
    }
}