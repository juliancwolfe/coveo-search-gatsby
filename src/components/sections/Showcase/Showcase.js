import React, { PureComponent } from 'react';
import { ResultCollection } from '../../domain';
import { Segment, Container } from 'semantic-ui-react';

export default class Showcase extends PureComponent {
    static defaultProps = {
        styles: {},
    };

    render() {
        const {
            styles, 
            items,
        } = this.props;

        return (
            <div className={styles['showcase']}>
                <Container fluid>
                    <Segment basic loading={!items}>
                        <ResultCollection items={items}/>
                    </Segment>
                </Container>
            </div>
        );
    }
}