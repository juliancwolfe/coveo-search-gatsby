export default class Filter {
    field;
    values;

    getId() {
        return this.field;
    }

    getField() {
        return this.field;
    }

    setField(field) {
        this.field = field;
        return this;
    }

    getValues() {
        return this.values;
    }

    setValues(values) {
        this.values = values;
        return this;
    }
}