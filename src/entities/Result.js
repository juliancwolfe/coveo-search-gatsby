export default class Result {
    id;
    title;
    uri;
    rating;
    thumbnailUri;
    price;
    producer;
    format;
    country;
    region;
    category;
    tasteLabels;
    aromaFamilies;
    
    getId() {
        return this.id;
    }
    
    setId(id) {
        this.id = id;
        return this;
    }

    getTitle() {
        return this.title;
    }

    setTitle(title) {
        this.title = title;
        return this;
    }

    getUri() {
        return this.uri;
    }

    setUri(uri) {
        this.uri = uri;
        return this;
    }

    getRating() {
        return this.rating;
    }

    setRating(rating) {
        this.rating = rating;
        return this;
    }

    getThumbnailUri() {
        return this.thumbnailUri;
    }

    setThumbnailUri(thumbnailUri) {
        this.thumbnailUri = thumbnailUri;
        return this;
    }

    getPrice() {
        return this.price;
    }

    setPrice(price) {
        this.price = price;
        return this;
    }

    getProducer() {
        return this.producer;
    }

    setProducer(producer) {
        this.producer = producer;
        return this;
    }

    getFormat() {
        return this.format;
    }

    setFormat(format) {
        this.format = format;
        return this;
    }

    getCountry() {
        return this.country;
    }

    setCountry(country) {
        this.country = country;
        return this;
    }

    getRegion() {
        return this.region;
    }

    setRegion(region) {
        this.region = region;
        return this;
    }

    getCategory() {
        return this.category;
    }

    setCategory(category) {
        this.category = category;
        return this;
    }

    getTasteLabels() {
        return this.tasteLabels;
    }

    setTasteLabels(tasteLabels = []) {
        this.tasteLabels = tasteLabels;
        return this;
    }

    getAromaFamilies() {
        return this.aromaFamilies;
    }

    setAromaFamilies(aromaFamilies = []) {
        this.aromaFamilies = aromaFamilies;
        return this;
    }
}