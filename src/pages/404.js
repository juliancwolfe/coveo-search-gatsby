import React, { PureComponent } from 'react';
import { NotFoundLayout } from "../layouts";

export default class NotFoundPage extends PureComponent {
    render() {
        return (
            <NotFoundLayout/>
        );
    }
}
