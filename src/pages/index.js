import React, { PureComponent } from 'react';
import { IndexLayout } from "../layouts";
import { SearchService } from '../services';
import { ApiProvider } from '../providers';
import { ResultRepository, FilterRepository } from '../repositories';
import { ResultFactory, FilterFactory } from '../factories';
import { search } from '../config/api';

export default class IndexPage extends PureComponent {
    state = {
        page: 1,
        totalResults: 0,
        limitValue: 18,
    };

    constructor(props) {
        super(props);

        this.searchService = new SearchService(
            new ResultRepository(new ApiProvider(search), new ResultFactory()),
            new FilterRepository(new ApiProvider(search), new FilterFactory()),
            {
                fieldsToInclude: [
                    'tpthumbnailuri',
                    'tpprixnum',
                    'tpproducteur',
                    'tpformat',
                    'tppays',
                    'tpcategorie',
                    'tpsousregion',
                    'tppastilledegoutsplitgroup',
                    'tpobservationsgustavivesfamillesaromessplitgroup'
                ],
            }
        );
    }

    async componentDidMount() {
        const qb = this.searchService.createQueryBuilder();
        const {limitValue, page} = this.state;
        qb.setLimit(limitValue).setPage(page);

        await this.getFilters();
        await this.refreshResults(qb);
        
        this.setState({
            qb,
        })
    }

    async handleSearch(query) {
        let {qb} = this.state;
        qb.setQuery(query);

        await this.refreshResults(qb);
    }

    async refreshResults(qb) {
        const items = await this.searchService.search(qb);
        this.setState({
            items,
        })
    }

    async getFilters() {
        const categories = await this.searchService.getFilters('tpcategorie', 20);
        const countries = await this.searchService.getFilters('tppays', 20);
        const stores = await this.searchService.getFilters('tpinventairenomsuccursalesplitgroup', 20);

        this.setState({
            categories,
            countries,
            stores,
        });
    }

    render() {
        const {
            items, 
            qb, 
            limitValue,
            categories,
            countries,
            stores,
        } = this.state;

        const limitValues = [18, 36, 48, 72, 100];

        return (
            <IndexLayout
                items={items}
                qb={qb}
                limitValue={limitValue}
                categories={categories}
                countries={countries}
                stores={stores}
                limitValues={limitValues}
                handleSearch={async query => await this.handleSearch(query)}
                refreshResults={async qb => await this.refreshResults(qb)}
            />
        );
    }
}
