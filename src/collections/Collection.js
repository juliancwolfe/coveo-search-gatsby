export default class Collection {
    total;
    items = new Map();

    getTotal() {
        return this.total;
    }
    
    setTotal(total) {
        this.total = total;
        return this;
    }

    getItems() {
        return this.items;
    }

    setItems(items) {
        this.items = items;
        return this;
    }

    addItem(item) {
        this.items.set(item.getId(), item);
    }

    forEach(cb) {
        return this.items.forEach(cb);
    }

    getValues() {
        return this.items.values();
    }
}