import Collection from './Collection';

export default class ResultCollection extends Collection {
    page;

    getPage() {
        return this.page;
    }

    setPage(page) {
        this.page = page;
        return this;
    }
}