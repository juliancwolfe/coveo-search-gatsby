import { ResultCollection } from "../collections";

export default class ResultRepository {
    constructor(provider, factory) {
        this.provider = provider;
        this.factory = factory;
    }

    async get(params) {
        const {data} = await this.provider.post('', params);

        const {results, totalCount} = data;
        const items = results.map(result => this.factory.create(result));

        return this.createCollection(items, totalCount);
    }

    createCollection(items, total) {
        return new ResultCollection().setItems(new Map(items.map(item => [item.getId(), item]))).setTotal(total);
    }
}