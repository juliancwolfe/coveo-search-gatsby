import { FilterCollection } from "../collections";

export default class FilterRepository {
    constructor(provider, factory) {
        this.provider = provider;
        this.factory = factory;
    }

    async get(params) {
        const {data} = await this.provider.post('', params);

        const {groupByResults, totalCount} = data;
        const items = groupByResults.map(result => this.factory.create(result));

        return this.createCollection(items, totalCount);
    }

    createCollection(items, total) {
        return new FilterCollection().setItems(new Map(items.map(item => [item.getId(), item]))).setTotal(total);
    }
}