import ResultRepository from './ResultRepository';
import FilterRepository from './FilterRepository';

export {
    ResultRepository,
    FilterRepository,
}